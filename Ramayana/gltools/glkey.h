#ifndef __GLKEY_H
#define __GLKEY_H

#define GLUT_KEY_ESCAPE			27
#define GLUT_KEY_DEL			127
#define GLUT_KEY_BACKSPACE		127
#define GLUT_KEY_RETURN			13
#define GLUT_CTRL_A				1
#define GLUT_CTRL_B				2
#define GLUT_CTRL_C				3
#define GLUT_CTRL_D				4
#define GLUT_CTRL_E				5
#define GLUT_CTRL_F				6
#define GLUT_CTRL_G				7
#define GLUT_CTRL_H				8
#define GLUT_CTRL_I				9
#define GLUT_CTRL_J				10
#define GLUT_CTRL_K				11
#define GLUT_CTRL_L				12
#define GLUT_CTRL_M				13
#define GLUT_CTRL_N				14
#define GLUT_CTRL_O				15
#define GLUT_CTRL_P				16
#define GLUT_CTRL_Q				17
#define GLUT_CTRL_R				18
#define GLUT_CTRL_S				19
#define GLUT_CTRL_T				20
#define GLUT_CTRL_U				21
#define GLUT_CTRL_V				22
#define GLUT_CTRL_W				23
#define GLUT_CTRL_X				24
#define GLUT_CTRL_Y				25
#define GLUT_CTRL_Z				26

#define GLUT_SCROLL_UP			3
#define GLUT_SCROLL_DOWN		4

#endif